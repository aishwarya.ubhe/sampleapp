import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

export default function Header() {
  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>My PlayList</Text>
    </View>
  )
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: 'green',
    height: 70,
  },
  headerText: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 15
  }
})