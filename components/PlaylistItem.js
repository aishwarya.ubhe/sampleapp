import React from 'react';
import { StyleSheet, Text,TouchableOpacity, View } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

export default function PlaylistUtem({ item, pressHandler }) {
  return (
    <TouchableOpacity onPress={() => pressHandler(item.key)}>
      <View style={styles.item}>
        <View style={styles.itemText}>
          <MaterialIcons name="delete" color='red' size={24} left={10} />
        </View>
        <Text >{item.name}</Text>
      </View>
    </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
  item: {
    marginTop: 16,
    padding: 16,
    flexDirection: 'row',
    // color: '#fff',
    borderWidth:1,
    borderColor:'#bbb',
    borderRadius: 10,
    // justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  itemText: {
    left: 250,
  }
})