import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

export default function AddPlaylist( { submitHandler} ) {
  const [textValue, setTextValue] = useState('')
  const changeHandler = (val) => {
    setTextValue(val);
  }
  return(
      <View>
        <TextInput
        style={styles.input}
        placeholder='Add Playlist here'
        onChangeText={val => changeHandler(val)}
        />
       <Button
         title='Add Playlist'
         color='green'
         onPress={() => submitHandler(textValue)}
       />
      </View>
    )
}

const styles = StyleSheet.create({
  input: {
    margin: 10,
    borderWidth: 1,
    borderColor: "#bbb"
  }
})