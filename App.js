import React, { useState} from 'react';
import { StyleSheet, Text, View, FlatList, Alert, TouchableWithoutFeedback, Keyboard } from 'react-native';
import Header from './components/Header'
import PlaylistItem from './components/PlaylistItem'
import AddPlaylist from './components/AddPlaylist'

export default function App() {
  const [playlist, setPlaylist] = useState([
  {name: 'slient', key:'1'},
  {name: 'rock', key:'2'},
  {name: 'bollywood', key:'3'},
  {name: 'english', key:'4'},
  {name: 'romantic', key:'5'},
  ])
  const pressHandler = (key) => {
    setPlaylist((prevPlaylist) => {
      return playlist.filter(pl => pl.key != key);
    })
  }
  const submitHandler = (text) => {
      if(text.length > 3 ) {
        setPlaylist((prevPlaylist) => {
          return [
          { name: text, key: Math.random().toString() },
          ...prevPlaylist
          ]
        })
    } else {
      Alert.alert('Error','Playlist name must be greater than 3 chacter',[
        {text: 'Ok', onPress: () => console.log('Alert closed')}
        ])
      }
    }

  return (
    <TouchableWithoutFeedback onPress={() => {
      Keyboard.dismiss();
      console.log('Keyboard dismiss');
    }}>
      <View style={styles.container}>
        <Header />
        <View style={styles.content}>
          <AddPlaylist submitHandler={submitHandler}/>
           <View style={styles.flatlist}>
              <FlatList
              data={playlist}
              renderItem={({item})=> (
                <PlaylistItem item={item} pressHandler={pressHandler} />
                )}
              />
            </View>
           </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    flex: 1,
    padding: 40,
    // backgroundColor: 'pink'
  },
  flatlist: {
    flex: 1,
    // backgroundColor: '#333',
  }
});
